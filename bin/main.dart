num add(num x, num y) {
    return x + y;
}

Map<String, num> switchNumber(num num1, num2) {
    
    num1 = num1 + num2;
    num2 = num1 - num2;
    num1 = num1 - num2;

    return {
        'num1': num1,
        'num2': num2
    };
}

int? countLetter(String letter, String sentence) {
    int result = 0;

    if(letter.length == 1){

        for(int i = 0; i<sentence.length; i++){
            if(sentence[i] == letter){
                result++;
            }
        }
        
        return result;
    } else {
        return null;
    }
    
}

bool isPalindrome(String text) {

    String editText = text.split(' ').join('');
    editText = editText.toLowerCase();
    String reverse = '';

    for(int i=0; i<editText.length; i++){
        reverse = reverse + editText[(editText.length-1) - i];
    }

    if(editText == reverse){
        return true;
    } else {
        return false;
    }
    
}

bool isIsogram(String text) {

    text = text.toLowerCase();

    for(int i = 0; i<text.length; i++){
        String letterCheck = text[i];

        for(int j = 0; j<text.length; j++){
            if(i!=j){
                if(letterCheck == text[j]){
                    return false;
                }
            }
        }
    }  

    return true;
}

double? purchase(int age, num price) {
    num discount = price*(0.80);

    if(age < 13){
        return null;
    } else if(age <= 21 || age >=65){
        return double.parse(discount.toStringAsFixed(2));
    } else {
        return double.parse(price.toStringAsFixed(2));
    }

}

List<String> findHotCategories(List items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].

    List<String> hotCategories = [];

    for(var item in items){
        if(item['stocks'] == 0){
            if(hotCategories.indexWhere((category) => item['category'] == category) == -1){
                hotCategories.add(item['category']);
            }
        }
    }
    return hotCategories;
}

List<String> findFlyingVoters(List candidateA, List candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    List<String> flyingVoters = [];

    for(var voter in candidateA){
        if(candidateB.indexWhere((each) => voter == each) != -1){
            flyingVoters.add(voter);
        }
    }

    return flyingVoters;
}

List<Map<String, dynamic>> calculateAdEfficiency(List items) {
    // Sort the ad efficiency according to the most to least efficient.

    // The passed campaigns array from the test are the following:
    // { brand: 'Brand X', expenditure: 12345.89, customersGained: 4879 }
    // { brand: 'Brand Y', expenditure: 22456.17, customersGained: 6752 }
    // { brand: 'Brand Z', expenditure: 18745.36, customersGained: 5823 }

    // The efficiency is computed as (customersGained / expenditure) x 100.

    // The expected output after processing the campaigns array are:
    // { brand: 'Brand X', adEfficiency: 39.51922461645131 }
    // { brand: 'Brand Z', adEfficiency: 31.063687227132476 }
    // { brand: 'Brand Y', adEfficiency: 30.06746030155632 }
    List<Map<String, dynamic>> ads = [];

    ads = items.map((campaign){
        return{
            'brand': campaign['brand'],
            'adEfficiency': ((campaign['customersGained'])/(campaign['expenditure']))*100
        };
    }).toList();

    ads.sort((b,a) => a['adEfficiency'].compareTo(b['adEfficiency']) 
    );
    
    return ads;
}